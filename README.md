# atlNotifier

Simple wrapper for node-notifier that uses async await and uses some basic defaults.

Takes all the same parameters as the original node-notifier.

See https://github.com/mikaelbr/node-notifier#usage-notificationcenter


## Install

```shell
npm install git+https://bitbucket.org/ATLTed/atl-notifier.git#master
```

## Example Usage

```javascript
const atlNotifier = require('atl-notifier')

run()

async function run(){
  let notification1 = await atlNotifier.notify({message: 'Do Something?', actions: ['Yes', 'Something Else']})

  console.log(notification1);

  let notification2 = await atlNotifier.reply({message: 'Do you want to reply to them?'})
  
  console.log(notification2);

  atlNotifier.simpleNotify({message: 'Simple Notification'})

  atlNotifier.simpleNotify("Other Simple Notification")
}
```