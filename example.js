const atlNotifier = require('./index.js')

run()

async function run(){
  let notification1 = await atlNotifier.notify({message: 'Do Something?', actions: ['Yes', 'Something Else']})

  console.log(notification1);

  let notification2 = await atlNotifier.reply({message: 'Do you want to reply to them?'})

  console.log(notification2);

  atlNotifier.simpleNotify({message: 'Simple Notification'})

  atlNotifier.simpleNotify("Other Simple Notification")
  
  
}