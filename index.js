
// https://github.com/mikaelbr/node-notifier#usage-notificationcenter
const notifier = require('node-notifier');

exports.reply = async function(n){
  n.reply = true
  return exports.notify(n)  
}

exports.simpleNotify = async function(n){
  n = typeof n === "string" ? {message:n} : n;
  let setDefault = ((k, v) => n[k] = n[k] === undefined ? v : n[k])
  setDefault('title', 'Node Notification');
  setDefault('sound', 'Hero');
  setDefault('wait', false);
  return notify(n);
}

exports.notify = async function(n){
  let setDefault = ((k, v) => n[k] = n[k] === undefined ? v : n[k])
  setDefault('title', 'Node Notification');
  setDefault('closeLabel', 'Cancel');
  setDefault('sound', 'Hero');
  setDefault('wait', true);
  setDefault('timeout', 15);
  return notify(n);
}

function notify(n){
  return new Promise((resolve, reject)=>{
    notifier.notify(n,(err, response, metadata)=>{
      resolve(response === 'timeout' ? undefined : metadata.activationValue)
    }); 
  })
}